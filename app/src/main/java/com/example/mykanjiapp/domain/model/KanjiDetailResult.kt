package com.example.mykanjiapp.domain.model

sealed class KanjiDetailResult {
    data class Success(val kanjiDetail: KanjiDomainModel) : KanjiDetailResult()
    data class Failure(val errorMessage: String) : KanjiDetailResult()
}