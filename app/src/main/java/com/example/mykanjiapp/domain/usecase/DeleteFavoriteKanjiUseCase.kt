package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DeleteFavoriteKanjiUseCase(private val repository: KanjiRepository) {
    suspend operator fun invoke(kanji: String): Flow<Unit> = flow {
        emit(repository.deleteFavoriteKanji(kanji))
    }
}