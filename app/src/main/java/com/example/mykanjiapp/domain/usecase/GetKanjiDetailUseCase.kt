package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.KanjiDetailResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetKanjiDetailUseCase(private val repository: KanjiRepository) {
    suspend operator fun invoke(kanji: String): Flow<KanjiDetailResult> =
        flow {
            emit(repository.getKanjiDetail(kanji))
        }
}