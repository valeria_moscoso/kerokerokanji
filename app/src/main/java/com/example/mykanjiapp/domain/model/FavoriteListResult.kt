package com.example.mykanjiapp.domain.model

data class FavoriteListResult(val favoriteList: List<String>)
