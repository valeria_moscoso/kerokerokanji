package com.example.mykanjiapp.domain.model

sealed class KanjiByGradeResult {
    data class Success(val kanjiList: List<KanjiDomainModel>) : KanjiByGradeResult()
    data class Failure(val errorMessage: String) : KanjiByGradeResult()
}