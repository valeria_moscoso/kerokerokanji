package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetKanjisByGradeUseCase(private val repository: KanjiRepository) {
    suspend operator fun invoke(grade: String): Flow<KanjiByGradeResult> =
        flow {
            emit(repository.getGradeKanjis(grade))
        }
}