package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.FavoriteListResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SaveFavoriteKanjiUseCase(private val repository: KanjiRepository) {
    suspend operator fun invoke(kanji: String): Flow<Unit> = flow {
        emit(repository.saveFavoriteKanji(kanji))
    }
}