package com.example.mykanjiapp.data.mapper

import com.example.mykanjiapp.data.model.KanjiApiModel
import com.example.mykanjiapp.domain.model.KanjiDomainModel

class CharacterMapper {
    fun fromCloudKanjiListToDomainList(characterList: List<String>): List<KanjiDomainModel> =
        characterList.map { KanjiDomainModel(it) }

    fun fromCloudKanjiDetailToDomainModel(kanjiApiModel: KanjiApiModel): KanjiDomainModel =
        KanjiDomainModel(
            kanji = kanjiApiModel.kanji,
            grade = kanjiApiModel.grade.toString(),
            strokeCount = kanjiApiModel.stroke_count,
            meanings = kanjiApiModel.meanings,
            heisigEn = kanjiApiModel.heisig_en,
            kunReadings = kanjiApiModel.kun_readings,
            onReadings = kanjiApiModel.onReadings,
            nameReadings = kanjiApiModel.name_readings,
            jlpt = kanjiApiModel.jlpt,
            unicode = kanjiApiModel.unicode
        )
}