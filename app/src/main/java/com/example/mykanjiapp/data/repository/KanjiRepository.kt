package com.example.mykanjiapp.data.repository

import com.example.mykanjiapp.data.datasource.FavoriteKanjiLocalDataSource
import com.example.mykanjiapp.data.datasource.KanjiCloudDataSource
import com.example.mykanjiapp.domain.model.FavoriteListResult
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDetailResult

class KanjiRepository(
    private val cloudDataSource: KanjiCloudDataSource,
    private val favoriteKanjiLocalDataSource: FavoriteKanjiLocalDataSource
) {

    suspend fun getGradeKanjis(grade: String): KanjiByGradeResult =
        cloudDataSource.getGradeKanjis(grade)

    suspend fun getKanjiDetail(kanji: String): KanjiDetailResult =
        when (val result = cloudDataSource.getKanjiDetail(kanji)) {
            is KanjiDetailResult.Success -> {
                val isFavorite = favoriteKanjiLocalDataSource.isKanjiFavorite(result.kanjiDetail.kanji)
                result.copy(result.kanjiDetail.copy(isFavorite = isFavorite))
            }
            else -> result
        }

    fun saveFavoriteKanji(kanji: String) {
        favoriteKanjiLocalDataSource.saveKanjiToFavourites(kanji)
    }

    fun deleteFavoriteKanji(kanji: String) {
        favoriteKanjiLocalDataSource.deleteKanjiFromFavorites(kanji)
    }

    fun getFavoritesKanji(): FavoriteListResult = favoriteKanjiLocalDataSource.getKanjiListFromFavourites()

}