package com.example.mykanjiapp.data.api

import com.example.mykanjiapp.data.model.KanjiApiModel
import retrofit2.http.GET
import retrofit2.http.Path

interface KanjiApi {

    @GET("grade-{gradeNumber}")
    suspend fun getGradeKanjis(
        @Path("gradeNumber") grade: String
    ): List<String>

    @GET("{character}")
    suspend fun getKanjiDetail(
        @Path("character") character: String
    ) :KanjiApiModel

}