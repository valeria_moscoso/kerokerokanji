package com.example.mykanjiapp.data.datasource

import com.example.mykanjiapp.data.db.FavoriteKanjiDao
import com.example.mykanjiapp.domain.model.FavoriteListResult

class FavoriteKanjiLocalDataSource(private val dao: FavoriteKanjiDao) {

    fun deleteKanjiFromFavorites(kanji: String) {
        dao.getFavoriteList().let {
            val mutableList = it.toMutableList()
            mutableList.find { currentList -> currentList == kanji }?.let { kanji -> mutableList.remove(kanji) }
            dao.saveFavorite(mutableList)
        }
    }

    fun saveKanjiToFavourites(kanji: String) {
        dao.getFavoriteList().let { currentList ->
            val newList = currentList.toMutableList()
            newList.add(kanji)
            dao.saveFavorite(newList)
        }
    }

    fun getKanjiListFromFavourites(): FavoriteListResult = FavoriteListResult(dao.getFavoriteList())

    fun isKanjiFavorite(kanji: String): Boolean =
        dao.getFavoriteList().contains(kanji)
}