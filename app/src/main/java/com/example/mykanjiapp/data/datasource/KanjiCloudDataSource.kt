package com.example.mykanjiapp.data.datasource

import com.example.mykanjiapp.data.api.KanjiApi
import com.example.mykanjiapp.data.mapper.CharacterMapper
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDetailResult

class KanjiCloudDataSource(
    private val kanjiApi: KanjiApi,
    private val mapper: CharacterMapper
) {
    suspend fun getGradeKanjis(grade: String): KanjiByGradeResult =
        runCatching {
            kanjiApi.getGradeKanjis(grade)
                .let {
                    KanjiByGradeResult.Success(mapper.fromCloudKanjiListToDomainList(it))
                }
        }.getOrElse { throwable ->
            KanjiByGradeResult.Failure(throwable.message ?: "")
        }

    suspend fun getKanjiDetail(character: String): KanjiDetailResult =
        runCatching {
            kanjiApi.getKanjiDetail(character).let {
                KanjiDetailResult.Success(mapper.fromCloudKanjiDetailToDomainModel(it))
            }
        }.getOrElse { throwable ->
            KanjiDetailResult.Failure(throwable.message ?: "")
        }
}