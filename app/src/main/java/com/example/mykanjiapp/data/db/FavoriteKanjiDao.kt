package com.example.mykanjiapp.data.db

import android.content.Context
import android.content.SharedPreferences

class FavoriteKanjiDao(private val context: Context) {

    private fun getDaoEditor(): SharedPreferences =
        context.getSharedPreferences(SHARED_PREFS_KANJIS, Context.MODE_PRIVATE)

    fun saveFavorite(setList: List<String>) {
        val editor = getDaoEditor().edit()
        editor.putStringSet(LIST_KEY, setList.toSet())
        editor.apply()
    }

    fun getFavoriteList(): List<String> =
        getDaoEditor().getStringSet(LIST_KEY, null)?.toList()?: emptyList()

    companion object {
        private const val SHARED_PREFS_KANJIS = "SHARED_PREFS_KANJIS"
        private const val LIST_KEY = "list_key"
    }
}