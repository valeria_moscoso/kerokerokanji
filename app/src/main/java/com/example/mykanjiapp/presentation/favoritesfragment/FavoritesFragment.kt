package com.example.mykanjiapp.presentation.favoritesfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mykanjiapp.R
import com.example.mykanjiapp.databinding.FragmentFavoritesBinding
import com.example.mykanjiapp.presentation.favoritesfragment.adapter.FavoritesAdapter
import com.example.mykanjiapp.presentation.kanjidetail.KanjiDetailFragment
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class FavoritesFragment : Fragment() {
    private lateinit var binding: FragmentFavoritesBinding
    private var kanjiAdapter: FavoritesAdapter? = null

    private val viewModel: FavoritesViewModel by activityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val fragmentBinding = FragmentFavoritesBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        initAdapter()
        setToolbar()
        getKanjiList()
        setObservers()
        return fragmentBinding.root
    }

    private fun setObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is FavoritesViewModel.FavoriteListState.Success -> handleSuccessState(state)
                is FavoritesViewModel.FavoriteListState.Empty -> handleEmptyState()
                is FavoritesViewModel.FavoriteListState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun handleSuccessState(state: FavoritesViewModel.FavoriteListState.Success) {
        setData(state.favoritesList)
        binding.errorView.visibility = View.GONE
        binding.progressBar.visibility = View.GONE
    }

    private fun handleEmptyState() {
        setData(emptyList())
        binding.errorView.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
    }

    private fun initAdapter() = with(binding.favoritesRecycler) {
        layoutManager = GridLayoutManager(requireContext(), 2)
        kanjiAdapter = FavoritesAdapter(mutableListOf(), ::onClick)
        adapter = kanjiAdapter
    }

    private fun setData(list: List<String>) {
        kanjiAdapter?.list = list
        kanjiAdapter?.notifyDataSetChanged()
    }

    private fun onClick(kanji: String) {
        val bundle = bundleOf(KanjiDetailFragment.DETAIL_KEY to kanji)
        findNavController().navigate(R.id.action_go_to_detail_from_favs, bundle)
    }

    private fun getKanjiList() {
        viewModel.getFavoritesKanji()
    }

    private fun setToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }
}