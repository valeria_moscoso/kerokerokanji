package com.example.mykanjiapp.presentation.favoritesfragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mykanjiapp.databinding.ItemCharacterFavsBinding

class FavoritesAdapter(
    var list: List<String>,
    private val onClick: (kanji: String) -> Unit
) : RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val binding = ItemCharacterFavsBinding.inflate(LayoutInflater.from(parent.context))
        return FavoritesViewHolder(binding, onClick)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    class FavoritesViewHolder(private val binding: ItemCharacterFavsBinding, private val onClick: (kanji: String) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(kanji: String) {
            binding.kanji.text = kanji
            binding.card.setOnClickListener {
                onClick.invoke(kanji)
            }
        }
    }
}
