package com.example.mykanjiapp.presentation.kanjilist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import com.example.mykanjiapp.domain.usecase.GetKanjisByGradeUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class KanjiListViewModel(
    private val getKanjisByGradeUseCase: GetKanjisByGradeUseCase
) : ViewModel() {
    var state = MutableLiveData<KanjiListState>().apply {
        KanjiListState.Loading
    }

    fun getKanjis(grade: String) {
        viewModelScope.launch {
            getKanjisByGradeUseCase.invoke(grade)
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    when (result) {
                        is KanjiByGradeResult.Failure ->
                            state.postValue(KanjiListState.Failure(result.errorMessage))
                        is KanjiByGradeResult.Success ->
                            state.postValue(KanjiListState.Success(result.kanjiList))
                    }
                }
        }
    }

    fun onDestroy() {
        state.postValue(
            KanjiListState.Success(emptyList())
        )
    }

    sealed class KanjiListState {
        object Loading : KanjiListState()
        data class Success(val list: List<KanjiDomainModel>) : KanjiListState()
        data class Failure(val message: String) : KanjiListState()
    }
}

