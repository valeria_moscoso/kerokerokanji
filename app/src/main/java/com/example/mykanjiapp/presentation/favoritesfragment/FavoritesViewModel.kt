package com.example.mykanjiapp.presentation.favoritesfragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mykanjiapp.domain.usecase.GetFavoritesKanjiUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class FavoritesViewModel(
    private val getFavoritesKanjiUseCase: GetFavoritesKanjiUseCase
) : ViewModel() {
    var state = MutableLiveData<FavoriteListState>().apply {
        FavoriteListState.Loading
    }

    fun getFavoritesKanji() {
        viewModelScope.launch {
            getFavoritesKanjiUseCase()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    state.postValue(checkEmptyList(result.favoriteList))
                }
        }
    }

    private fun checkEmptyList(favoriteList: List<String>) =
        if (favoriteList.isNotEmpty()) {
            FavoriteListState.Success(favoriteList)
        } else {
            FavoriteListState.Empty
        }

    fun onDestroy() {
        state.postValue(
            FavoriteListState.Success(emptyList())
        )
    }

    sealed class FavoriteListState {
        object Loading : FavoriteListState()
        object Empty : FavoriteListState()
        data class Success(val favoritesList: List<String>) : FavoriteListState()
    }

}