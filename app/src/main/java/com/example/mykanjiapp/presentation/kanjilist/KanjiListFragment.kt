package com.example.mykanjiapp.presentation.kanjilist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mykanjiapp.R
import com.example.mykanjiapp.databinding.FragmentKanjiListBinding
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import com.example.mykanjiapp.presentation.kanjidetail.KanjiDetailFragment
import com.example.mykanjiapp.presentation.kanjilist.adapter.KanjiAdapter
import org.koin.androidx.viewmodel.ext.android.activityViewModel


class KanjiListFragment : Fragment() {

    private lateinit var binding: FragmentKanjiListBinding
    private var kanjiAdapter: KanjiAdapter? = null
    private val viewModel: KanjiListViewModel by activityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val fragmentBinding = FragmentKanjiListBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        initAdapter()
        initObservers()
        requestData()
        setToolbar()
        return fragmentBinding.root
    }

    private fun initAdapter() = with(binding.recycler) {
        layoutManager = GridLayoutManager(requireContext(), 4)
        kanjiAdapter = KanjiAdapter(mutableListOf(), ::onClick)
        adapter = kanjiAdapter
    }

    private fun initObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is KanjiListViewModel.KanjiListState.Failure -> {
                    binding.errorView.text = state.message
                    binding.errorView.visibility = View.VISIBLE
                    binding.kanjiListContainer.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                }
                is KanjiListViewModel.KanjiListState.Success -> {
                    binding.errorView.visibility = View.GONE
                    binding.kanjiListContainer.visibility = View.VISIBLE
                    setData(state.list)
                    binding.progressBar.visibility = View.GONE
                }
                is KanjiListViewModel.KanjiListState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun requestData() {
        val grade = arguments?.getString(GRADE_KEY)
        grade?.let { viewModel.getKanjis(it) }
    }

    private fun setData(list: List<KanjiDomainModel>) {
        kanjiAdapter?.list = list
        kanjiAdapter?.notifyDataSetChanged()
    }

    private fun setToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun onClick(kanji: String) {
        val bundle = bundleOf(KanjiDetailFragment.DETAIL_KEY to kanji)
        findNavController().navigate(R.id.action_go_to_detail, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    companion object {
        const val GRADE_KEY = "grade"
    }
}