package com.example.mykanjiapp.presentation.kanjilist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mykanjiapp.databinding.ItemCharacterBinding
import com.example.mykanjiapp.domain.model.KanjiDomainModel

class KanjiAdapter(
    var list: List<KanjiDomainModel>,
    private val onClick: (kanji: String) -> Unit
) : RecyclerView.Adapter<KanjiAdapter.KanjiViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KanjiViewHolder {
        val binding = ItemCharacterBinding.inflate(LayoutInflater.from(parent.context))
        return KanjiViewHolder(binding, onClick)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: KanjiViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    class KanjiViewHolder(private val binding: ItemCharacterBinding, private val onClick: (kanji: String) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(model: KanjiDomainModel) {
            binding.kanji.text = model.kanji
            binding.card.setOnClickListener {
                onClick.invoke(model.kanji)
            }
        }
    }
}
