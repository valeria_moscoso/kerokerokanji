package com.example.mykanjiapp.presentation.kanjidetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mykanjiapp.domain.model.KanjiDetailResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import com.example.mykanjiapp.domain.usecase.DeleteFavoriteKanjiUseCase
import com.example.mykanjiapp.domain.usecase.GetKanjiDetailUseCase
import com.example.mykanjiapp.domain.usecase.SaveFavoriteKanjiUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class KanjiDetailViewModel(
    private val getKanjiDetailUseCase: GetKanjiDetailUseCase,
    private val saveFavoriteKanjiUseCase: SaveFavoriteKanjiUseCase,
    private val deleteFavoriteKanjiUseCase: DeleteFavoriteKanjiUseCase
) : ViewModel() {
    var state = MutableLiveData<KanjiDetailState>().apply {
        KanjiDetailState.Loading
    }

    fun getKanjiDetail(kanji: String) {
        viewModelScope.launch {
            getKanjiDetailUseCase(kanji)
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    when (result) {
                        is KanjiDetailResult.Failure ->
                            state.postValue(KanjiDetailState.Failure(result.errorMessage))
                        is KanjiDetailResult.Success ->
                            state.postValue(KanjiDetailState.Success(result.kanjiDetail))
                    }
                }
        }
    }

    fun saveToFavorite(kanji: String) {
        viewModelScope.launch {
            saveFavoriteKanjiUseCase(kanji)
                .flowOn(Dispatchers.IO)
                .collect()
        }
    }

    fun deleteFromFavorite(kanji: String) {
        viewModelScope.launch {
            deleteFavoriteKanjiUseCase(kanji)
                .flowOn(Dispatchers.IO)
                .collect()
        }
    }

    fun onDestroy() {
        state.postValue(KanjiDetailState.Cleared)
    }

    sealed class KanjiDetailState {
        object Loading : KanjiDetailState()
        object Cleared : KanjiDetailState()
        data class Success(val model: KanjiDomainModel) : KanjiDetailState()
        data class Failure(val message: String) : KanjiDetailState()
    }

}