package com.example.mykanjiapp.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mykanjiapp.R
import com.example.mykanjiapp.databinding.FragmentHomeBinding
import com.example.mykanjiapp.presentation.kanjilist.KanjiListFragment.Companion.GRADE_KEY

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val fragmentBiding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = fragmentBiding
        initListeners()
        initFavouritesButtonListener()
        return fragmentBiding.root
    }

    private fun initListeners() =
        with(binding) {
            listOf(
                gradeOneButton,
                gradeTwoButton,
                gradeThreeButton,
                gradeFourButton,
                gradeFiveButton,
                gradeSixButton
            ).forEach { button ->
                buttonClickListener(button)
            }
        }

    private fun buttonClickListener(button: Button) {
        button.setOnClickListener {
            val bundle = bundleOf(GRADE_KEY to button.tag)
            findNavController().navigate(R.id.action_go_to_Kanji_list, bundle)
        }
    }

    private fun initFavouritesButtonListener() {
        binding.favouritesButton.setOnClickListener {
            findNavController().navigate(R.id.action_go_to_favorites)
        }
    }
}