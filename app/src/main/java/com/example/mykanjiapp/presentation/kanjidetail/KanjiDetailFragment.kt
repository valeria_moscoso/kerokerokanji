package com.example.mykanjiapp.presentation.kanjidetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mykanjiapp.R
import com.example.mykanjiapp.databinding.FragmentKanjiDetailBinding
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class KanjiDetailFragment : Fragment() {

    private lateinit var binding: FragmentKanjiDetailBinding
    private val viewModel: KanjiDetailViewModel by activityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val fragmentBinding = FragmentKanjiDetailBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        setToolbar()
        val kanji = arguments?.getString(DETAIL_KEY)
        kanji?.let { viewModel.getKanjiDetail(it) }
        setObservers()
        return fragmentBinding.root
    }

    private fun setObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is KanjiDetailViewModel.KanjiDetailState.Failure -> {
                    binding.errorView.text = state.message
                    binding.descriptionContainer.visibility = View.GONE
                    binding.errorView.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }
                is KanjiDetailViewModel.KanjiDetailState.Success -> {
                    setData(state.model)
                    binding.descriptionContainer.visibility = View.VISIBLE
                    binding.errorView.visibility = View.GONE
                    favoriteButtonsListeners(state.model.kanji)
                    handleFavouriteButton(state.model.isFavorite)
                    binding.progressBar.visibility = View.GONE
                }
                is KanjiDetailViewModel.KanjiDetailState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is KanjiDetailViewModel.KanjiDetailState.Cleared -> {
                    binding.errorView.visibility = View.GONE
                    binding.descriptionContainer.visibility = View.GONE
                }
            }
        }
    }

    private fun setData(model: KanjiDomainModel) = with(binding) {
        kanji.text = model.kanji
        val meanings = model.meanings?.joinToString(separator = ", ")
        meaningsContent.text = if (meanings.isNullOrEmpty().not()) meanings else getString(R.string.detail_no_meanings)
        val kunReadings = model.kunReadings?.joinToString(separator = ", ")
        kunReadingsContent.text = if (kunReadings.isNullOrEmpty().not()) kunReadings else getString(R.string.detail_no_kun)
        val onReadings = model.onReadings?.joinToString(separator = ", ")
        onReadingsContent.text = if (onReadings.isNullOrEmpty().not()) onReadings else getString(R.string.detail_no_on)
        gradeContent.text = model.grade
        strokesContent.text = model.strokeCount.toString()
        jlptContent.text = model.jlpt.toString()
        heisingContent.text = model.heisigEn
        val nameReadings = model.nameReadings?.joinToString(separator = ", ")
        nameReadingsContent.text = if (nameReadings.isNullOrEmpty().not()) nameReadings else getString(R.string.detail_no_names)
    }

    private fun setToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun favoriteButtonsListeners(kanji: String) {
        binding.favoriteButton.setOnClickListener {
            viewModel.saveToFavorite(kanji)
            binding.favoriteButton.visibility = View.GONE
            binding.deleteFavoriteButton.visibility = View.VISIBLE
        }

        binding.deleteFavoriteButton.setOnClickListener {
            viewModel.deleteFromFavorite(kanji)
            binding.favoriteButton.visibility = View.VISIBLE
            binding.deleteFavoriteButton.visibility = View.GONE
        }
    }


    private fun handleFavouriteButton(isFavorite: Boolean) {
        if (isFavorite) {
            binding.favoriteButton.visibility = View.GONE
            binding.deleteFavoriteButton.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    companion object {
        const val DETAIL_KEY = "detail_key"
    }
}