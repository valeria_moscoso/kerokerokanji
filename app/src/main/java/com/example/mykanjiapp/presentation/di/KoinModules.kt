package com.example.mykanjiapp.presentation.di

import com.example.mykanjiapp.data.api.RetrofitAdapter
import com.example.mykanjiapp.data.datasource.FavoriteKanjiLocalDataSource
import com.example.mykanjiapp.data.datasource.KanjiCloudDataSource
import com.example.mykanjiapp.data.db.FavoriteKanjiDao
import com.example.mykanjiapp.data.mapper.CharacterMapper
import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.usecase.DeleteFavoriteKanjiUseCase
import com.example.mykanjiapp.domain.usecase.GetFavoritesKanjiUseCase
import com.example.mykanjiapp.domain.usecase.GetKanjiDetailUseCase
import com.example.mykanjiapp.domain.usecase.GetKanjisByGradeUseCase
import com.example.mykanjiapp.domain.usecase.SaveFavoriteKanjiUseCase
import com.example.mykanjiapp.presentation.kanjidetail.KanjiDetailViewModel
import com.example.mykanjiapp.presentation.kanjilist.KanjiListViewModel
import com.example.mykanjiapp.presentation.favoritesfragment.FavoritesViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val appModule = module {
    single { RetrofitAdapter.createKanjiApiRetrofit() }
    factory { CharacterMapper() }
    single { FavoriteKanjiDao(context = androidContext()) }
    factory { FavoriteKanjiLocalDataSource(dao = get()) }
    factory { KanjiCloudDataSource(kanjiApi = get(), mapper = get()) }
    factory { KanjiRepository(cloudDataSource = get(), favoriteKanjiLocalDataSource = get()) }
    factory { DeleteFavoriteKanjiUseCase(repository = get()) }
    factory { SaveFavoriteKanjiUseCase(repository = get()) }
    factory { GetFavoritesKanjiUseCase(repository = get()) }
    factory { GetKanjisByGradeUseCase(repository = get()) }
    factory { GetKanjiDetailUseCase(repository = get()) }
    viewModelOf(::KanjiDetailViewModel)
    viewModelOf(::KanjiListViewModel)
    viewModelOf(::FavoritesViewModel)
}




