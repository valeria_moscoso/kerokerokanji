package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class DeleteFavoriteKanjiUseCaseTest {
    private val kanjiRepository: KanjiRepository = mockk()
    val useCase = DeleteFavoriteKanjiUseCase(kanjiRepository)

    @Test
    fun `GIVEN KanjiRepository WHEN DeleteFavoriteKanjiUseCase is called THEN kanji is deleted`() =
        runBlocking {
            coEvery { kanjiRepository.deleteFavoriteKanji(KANJI) } returns Unit

            val response: Unit = useCase(KANJI).single()

            assertEquals(response, Unit)
        }

    private companion object {
        const val KANJI = "人"
    }
}