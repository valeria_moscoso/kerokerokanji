package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.KanjiDetailResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GetKanjiDetailUseCaseTest {
    private val kanjiRepository: KanjiRepository = mockk()
    val useCase = GetKanjiDetailUseCase(kanjiRepository)

    @Test
    fun `GIVEN KanjiRepository WHEN GetKanjiDetailUseCase is called THEN receive success response`() =
        runBlocking {
            coEvery { kanjiRepository.getKanjiDetail(KANJI) } returns SUCCESS_RESULT

            val response: KanjiDetailResult = useCase(KANJI).single()

            assertEquals(response, SUCCESS_RESULT)
        }

    @Test
    fun `GIVEN KanjiRepository WHEN GetKanjiDetailUseCase is called THEN receive failure response`() =
        runBlocking {
            coEvery { kanjiRepository.getKanjiDetail(KANJI) } returns FAILURE_RESULT

            val response: KanjiDetailResult = useCase(KANJI).single()

            assertEquals(response, FAILURE_RESULT)
        }

    private companion object {
        const val KANJI = "人"
        val KANJI_DOMAIN_MODEL = KanjiDomainModel("人")
        val SUCCESS_RESULT: KanjiDetailResult.Success = KanjiDetailResult.Success(kanjiDetail = KANJI_DOMAIN_MODEL)
        val FAILURE_RESULT: KanjiDetailResult.Failure = KanjiDetailResult.Failure(errorMessage = "error")
    }
}