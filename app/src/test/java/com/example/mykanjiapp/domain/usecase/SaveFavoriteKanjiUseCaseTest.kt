package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class SaveFavoriteKanjiUseCaseTest {
    private val kanjiRepository: KanjiRepository = mockk()
    val useCase = SaveFavoriteKanjiUseCase(kanjiRepository)

    @Test
    fun `GIVEN KanjiRepository WHEN SaveFavoriteKanjiUseCase is called THEN kanji is saved as favorite`() =
        runBlocking {
            coEvery { kanjiRepository.saveFavoriteKanji(KANJI) } returns Unit

            val response: Unit = useCase(KANJI).single()

            assertEquals(response, Unit)
        }

    private companion object {
        const val KANJI = "人"
    }
}