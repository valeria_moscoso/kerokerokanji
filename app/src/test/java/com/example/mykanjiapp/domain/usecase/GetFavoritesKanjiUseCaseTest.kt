package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.FavoriteListResult
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GetFavoritesKanjiUseCaseTest {
    private val kanjiRepository: KanjiRepository = mockk()
    val useCase = GetFavoritesKanjiUseCase(kanjiRepository)

    @Test
    fun `GIVEN KanjiRepository WHEN GetFavoritesKanjiUseCase is called THEN receive success response`() =
        runBlocking {
            coEvery { kanjiRepository.getFavoritesKanji() } returns SUCCESS_RESULT

            val response = useCase().single()

            assertEquals(response, SUCCESS_RESULT)
        }

    private companion object {
        const val KANJI = "人"
        val SUCCESS_RESULT = FavoriteListResult(favoriteList = listOf(KANJI))
    }

}