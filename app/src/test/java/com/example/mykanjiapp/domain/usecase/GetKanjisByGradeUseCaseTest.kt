package com.example.mykanjiapp.domain.usecase

import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GetKanjisByGradeUseCaseTest {

    private val kanjiRepository: KanjiRepository = mockk()
    val useCase = GetKanjisByGradeUseCase(kanjiRepository)

    @Test
    fun `GIVEN KanjiRepository WHEN GetKanjisByGradeUseCase is called THEN receive success response`() =
        runBlocking {
            coEvery { kanjiRepository.getGradeKanjis(GRADE) } returns SUCCESS_RESULT

            val response: KanjiByGradeResult = useCase(GRADE).single()

            assertEquals(response, SUCCESS_RESULT)
        }

    @Test
    fun `GIVEN KanjiRepository WHEN GetKanjisByGradeUseCase is called THEN receive failure response`() =
        runBlocking {
            coEvery { kanjiRepository.getGradeKanjis(GRADE) } returns FAILURE_RESULT

            val response: KanjiByGradeResult = useCase(GRADE).single()

            assertEquals(response, FAILURE_RESULT)
        }

    private companion object {
        const val GRADE = "1"
        val KANJI_DOMAIN_MODEL = KanjiDomainModel("人")
        val SUCCESS_RESULT: KanjiByGradeResult.Success = KanjiByGradeResult.Success(kanjiList = listOf(KANJI_DOMAIN_MODEL))
        val FAILURE_RESULT: KanjiByGradeResult.Failure = KanjiByGradeResult.Failure(errorMessage = "error")
    }
}