package com.example.mykanjiapp.data

import com.example.mykanjiapp.data.datasource.FavoriteKanjiLocalDataSource
import com.example.mykanjiapp.data.datasource.KanjiCloudDataSource
import com.example.mykanjiapp.data.repository.KanjiRepository
import com.example.mykanjiapp.domain.model.FavoriteListResult
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDetailResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class KanjiRepositoryTest {
    private val cloudDataSource: KanjiCloudDataSource = mockk()
    private val favoriteKanjiLocalDataSource: FavoriteKanjiLocalDataSource = mockk()
    private val repository = KanjiRepository(cloudDataSource, favoriteKanjiLocalDataSource)

    @Test
    fun `GIVEN a success result from datasource WHEN calling repository THEN returns expected kanjigradeResult`() = runBlocking {
        coEvery { cloudDataSource.getGradeKanjis(GRADE) } returns SUCCESS_RESULT

        val result = repository.getGradeKanjis(GRADE)

        assertEquals(SUCCESS_RESULT, result)
    }

    @Test
    fun `GIVEN a failure result from datasource WHEN calling repository THEN returns expected kanjigradeResult`() = runBlocking {
        coEvery { cloudDataSource.getGradeKanjis(GRADE) } returns FAILURE_RESULT

        val result = repository.getGradeKanjis(GRADE)

        assertEquals(FAILURE_RESULT, result)
    }

    @Test
    fun `GIVEN a success kanji detail result from datasource WHEN calling repository THEN returns expected kanji detail`() =
        runBlocking {
            coEvery { cloudDataSource.getKanjiDetail(KANJI) } returns SUCCESS_RESULT_DETAIL
            coEvery { favoriteKanjiLocalDataSource.isKanjiFavorite(KANJI) } returns false

            val result = repository.getKanjiDetail(KANJI)

            assertEquals(SUCCESS_RESULT_DETAIL, result)
        }

    @Test
    fun `GIVEN success from datasource WHEN calling repository THEN kanji is saved in favorites`() = runBlocking {
        coEvery { favoriteKanjiLocalDataSource.saveKanjiToFavourites(KANJI) } returns Unit

        val result = repository.saveFavoriteKanji(KANJI)

        assertEquals(Unit, result)
    }

    @Test
    fun `GIVEN success from datasource WHEN calling repository THEN kanji is deleted from favorites`() = runBlocking {
        coEvery { favoriteKanjiLocalDataSource.deleteKanjiFromFavorites(KANJI) } returns Unit

        val result = repository.deleteFavoriteKanji(KANJI)

        assertEquals(Unit, result)
    }

    @Test
    fun `GIVEN success from datasource WHEN calling repository THEN get a favorites stored list`() = runBlocking {
        coEvery { favoriteKanjiLocalDataSource.getKanjiListFromFavourites() } returns SUCCESS_RESULT_FAVORITES

        val result = repository.getFavoritesKanji()

        assertEquals(SUCCESS_RESULT_FAVORITES, result)
    }

    private companion object {
        const val GRADE = "1"
        const val KANJI = "人"
        val KANJI_DOMAIN_MODEL = KanjiDomainModel(KANJI)
        val SUCCESS_RESULT: KanjiByGradeResult.Success = KanjiByGradeResult.Success(kanjiList = listOf(KANJI_DOMAIN_MODEL))
        val FAILURE_RESULT: KanjiByGradeResult.Failure = KanjiByGradeResult.Failure(errorMessage = "error")
        val SUCCESS_RESULT_DETAIL: KanjiDetailResult.Success = KanjiDetailResult.Success(kanjiDetail = KANJI_DOMAIN_MODEL)
        val SUCCESS_RESULT_FAVORITES: FavoriteListResult = FavoriteListResult(favoriteList = listOf(KANJI))
    }
}