package com.example.mykanjiapp.presentation.kanjilist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mykanjiapp.domain.model.KanjiByGradeResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import com.example.mykanjiapp.domain.usecase.GetKanjisByGradeUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class KanjiListViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val getKanjisByGradeUseCase: GetKanjisByGradeUseCase = mockk()
    private lateinit var viewModel: KanjiListViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        viewModel = createViewModelInstance()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun createViewModelInstance() = KanjiListViewModel(
        getKanjisByGradeUseCase = getKanjisByGradeUseCase
    )

    @Test
    fun `GIVEN a success result WHEN calling use case THEN state contains expected content`() =
        runBlocking {
            coEvery { getKanjisByGradeUseCase.invoke(GRADE) } returns flowOf(SUCCESS_RESULT)

            viewModel.getKanjis(GRADE)

            assertEquals(viewModel.state.value, EXPECTED_SUCCESS_STATE)
        }

    @Test
    fun `GIVEN a success result WHEN calling onDestroy THEN state contains expected empty list`() {
        runBlocking {
            viewModel.onDestroy()

            assertEquals(viewModel.state.value, KanjiListViewModel.KanjiListState.Success(emptyList()))
        }
    }

    private companion object {
        const val GRADE = "1"
        val KANJI_DOMAIN_MODEL = KanjiDomainModel("人")
        val KANJI_LIST = listOf(KANJI_DOMAIN_MODEL)
        val SUCCESS_RESULT = KanjiByGradeResult.Success(kanjiList = KANJI_LIST)
        val EXPECTED_SUCCESS_STATE = KanjiListViewModel.KanjiListState.Success(KANJI_LIST)
    }
}