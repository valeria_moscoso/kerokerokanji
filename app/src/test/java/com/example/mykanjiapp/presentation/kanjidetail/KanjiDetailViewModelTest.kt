package com.example.mykanjiapp.presentation.kanjidetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mykanjiapp.domain.model.KanjiDetailResult
import com.example.mykanjiapp.domain.model.KanjiDomainModel
import com.example.mykanjiapp.domain.usecase.DeleteFavoriteKanjiUseCase
import com.example.mykanjiapp.domain.usecase.GetKanjiDetailUseCase
import com.example.mykanjiapp.domain.usecase.SaveFavoriteKanjiUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class KanjiDetailViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val getKanjiDetailUseCase: GetKanjiDetailUseCase = mockk()
    private val saveFavoriteKanjiUseCase: SaveFavoriteKanjiUseCase = mockk()
    private val deleteFavoriteKanjiUseCase: DeleteFavoriteKanjiUseCase = mockk()
    private lateinit var viewModel: KanjiDetailViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        viewModel = createViewModelInstance()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun createViewModelInstance() = KanjiDetailViewModel(
        getKanjiDetailUseCase = getKanjiDetailUseCase,
        saveFavoriteKanjiUseCase = saveFavoriteKanjiUseCase,
        deleteFavoriteKanjiUseCase = deleteFavoriteKanjiUseCase
    )

    @Test
    fun `GIVEN a success result WHEN calling use case THEN state contains expected content`() =
        runBlocking {
            coEvery { getKanjiDetailUseCase.invoke(KANJI) } returns flowOf(SUCCESS_RESULT)

            viewModel.getKanjiDetail(KANJI)

            assertEquals(viewModel.state.value, EXPECTED_SUCCESS_STATE)
        }

    @Test
    fun `GIVEN a success result WHEN calling onDestroy THEN state contains expected empty object`() {
        runBlocking {
            viewModel.onDestroy()

            assertEquals(
                viewModel.state.value, KanjiDetailViewModel.KanjiDetailState.Cleared
            )
        }
    }

    private companion object {
        const val KANJI = "人"
        val KANJI_DOMAIN_MODEL = KanjiDomainModel("人")
        val SUCCESS_RESULT = KanjiDetailResult.Success(kanjiDetail = KANJI_DOMAIN_MODEL)
        val EXPECTED_SUCCESS_STATE = KanjiDetailViewModel.KanjiDetailState.Success(KANJI_DOMAIN_MODEL)
    }
}