package com.example.mykanjiapp.presentation.kanjifavorites

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mykanjiapp.domain.model.FavoriteListResult
import com.example.mykanjiapp.domain.usecase.GetFavoritesKanjiUseCase
import com.example.mykanjiapp.presentation.favoritesfragment.FavoritesViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class FavoritesViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val getFavoritesKanjiUseCase: GetFavoritesKanjiUseCase = mockk()
    private lateinit var viewModel: FavoritesViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        viewModel = createViewModelInstance()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun createViewModelInstance() = FavoritesViewModel(
        getFavoritesKanjiUseCase = getFavoritesKanjiUseCase
    )

    @Test
    fun `GIVEN a success result WHEN calling usecase THEN state contains expected favorites list`() =
        runBlocking {
            coEvery { getFavoritesKanjiUseCase.invoke() } returns flowOf(SUCCESS_RESULT)

            viewModel.getFavoritesKanji()

            assertEquals(viewModel.state.value, EXPECTED_SUCCESS_STATE)

        }

    @Test
    fun `GIVEN a success result WHEN calling usecase THEN state contains expected empty list`() =
        runBlocking {
            coEvery { getFavoritesKanjiUseCase.invoke() } returns flowOf(SUCCESS_RESULT_EMPTY)

            viewModel.getFavoritesKanji()

            assertEquals(viewModel.state.value, EXPECTED_SUCCESS_EMPTY_STATE)

        }

    private companion object {
        val FAVORITES_LIST = listOf("人")
        val SUCCESS_RESULT = FavoriteListResult(FAVORITES_LIST)
        val SUCCESS_RESULT_EMPTY = FavoriteListResult(listOf())
        val EXPECTED_SUCCESS_STATE = FavoritesViewModel.FavoriteListState.Success(FAVORITES_LIST)
        val EXPECTED_SUCCESS_EMPTY_STATE = FavoritesViewModel.FavoriteListState.Empty
    }

}