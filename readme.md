Kero kero kanji is an application for students of Japanese with a medium-high level who already know how to read the two alphabets 
katakana and hiragana and have already started to study kanji. 

Architecture choice: 
* Single activity with fragments and navigation Component.
* The communication between fragment and viewModel has been implemented in a reactive way using MVVM, with the help of liveData and state handling.
* Threading is handled using the viewModel scope and flow to ensure a flexible thread selection, stream data and better exception handling if needed.
* Communication between layers is done in a Clean architecture way, where presentation only uses use cases from domain, and domain uses data as a way of encapsulating the domain model.
* A Result class has been used to handle all possible data exceptions in an easy way to render via State, this Result model is a domain model used until usecase where is mapped to State.
* To perform api calls the app uses Retrofit APi service with okhttpClient.
